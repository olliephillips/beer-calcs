<?php
$startingGravity = htmlspecialchars($_REQUEST['startinggravity'], ENT_QUOTES, 'UTF-8');
$finalGravity = htmlspecialchars($_REQUEST['finalgravity'], ENT_QUOTES, 'UTF-8');
require('../libraries/yeastAttenuation.php');
$att = yeastAttenuation::calcAttenuation($startingGravity, $finalGravity);
echo $att;