<?php
$temperature = htmlspecialchars($_REQUEST['temperature'], ENT_QUOTES, 'UTF-8');
$units = htmlspecialchars($_REQUEST['units'], ENT_QUOTES, 'UTF-8');
$hydrometer = htmlspecialchars($_REQUEST['hydrometer'], ENT_QUOTES, 'UTF-8');
require('../libraries/specificGravityCorrection.php');
$corrected = specificGravityCorrection::correctSG($hydrometer, $temperature, $units);
echo $corrected;