<?php
$startingGravity = htmlspecialchars($_REQUEST['startinggravity'], ENT_QUOTES, 'UTF-8');
$finalGravity = htmlspecialchars($_REQUEST['finalgravity'], ENT_QUOTES, 'UTF-8');
require('../libraries/alcoholByVolume.php');
$abv = alcoholByVolume::calcABV($startingGravity, $finalGravity);
echo $abv;