<?php 
class specificGravityCorrection {
	
	private static $temperature;
	private static $hydrometer;
	private static $plato;
	private static $adjPlato;
	
	public static function correctSG($hydrometer, $temperature, $units) {
		
		$coeffic = array(
						array(56.084, -0.17885, -0.13063),
						array(69.685, -1.367, -0.10621),
						array(77.782, -1.7288, -0.10822),
						array(87.895, -2.3601, -0.10285),
						array(97.052, -2.7729, -0.10596)
						);
		
		if($units == 'f') {
			$temperature = self::convertCelsius($temperature);
		}	
		
		self::$temperature = $temperature;
		self::$hydrometer = $hydrometer/1000;		
		self::$plato = self::toPlato();
		
	   	$coefficIndex = 4;
		if (self::$plato < 20){
			$coefficIndex = 3;
		} elseif (self::$plato < 15) {
			$coefficIndex = 2;
		}	
		if (self::$plato < 10) {
			$coefficIndex = 1;
		}	
		if (self::$plato < 5) {
			$coefficIndex = 0;
		}
			
	   	$deltaSG = ($coeffic[$coefficIndex][0])
	                   + ($coeffic[$coefficIndex][1] * self::$temperature)
	                   + ($coeffic[$coefficIndex][2] * self::$temperature * self::$temperature);
	
		self::$adjPlato = self::$plato - ($deltaSG/100);
	  	$correctedSG = self::toStandardGravity();
	   	return $correctedSG;
		
	}
	
	protected static function convertCelsius($f) {
		$c =  (5 / 9) * ($f - 32);
		return $c;
	}	
	
	protected static function toPlato() {
		$plato = -616.989 + (1111.488 * self::$hydrometer) - (630.606 * self::$hydrometer * self::$hydrometer) + (136.10305 * self::$hydrometer * self::$hydrometer * self::$hydrometer);
		return $plato;
	
	}
	
	protected static function toStandardGravity() {
		 $sg = 1.0000131 + (0.00386777 * self::$adjPlato) + (1.27447E-5 * self::$adjPlato * self::$adjPlato) + (6.34964E-8 * self::$adjPlato * self::$adjPlato * self::$adjPlato);
		 return number_format(round($sg,3), 3);
	}
}
