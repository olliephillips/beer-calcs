<?php
class yeastAttenuation {
	
	public static function getYeastOptions() {
		$JSON = file_get_contents("../json/yeasts.json");
		$yeasts = json_decode($JSON);
		$html ='';
		$curLab = '';
		foreach ($yeasts as $yeast) {
			if(($curLab != $yeast->lab) && ($curLab != '')){
				$html.= '</optigroup>';
			}	
			if($curLab != $yeast->lab) {
		   		$html.= '<optgroup label="'.$yeast->lab.'">';
			}
		   	$html.='<option value="'.$yeast->atten.'">'.$yeast->name.'</option>';
			$curLab = $yeast->lab;
		}
		return $html;
	}
	
	public static function calcAttenuation($startingGravity, $finalGravity) {
		//[(OG-FG)/(OG-1)] x 100
		$att = (($startingGravity - $finalGravity) / ($startingGravity - 1000)) * 100;
		return round($att).'%';
	}	
}