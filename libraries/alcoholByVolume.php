<?php
class alcoholByVolume {
	
	public static function calcABV($startingGravity, $finalGravity) {
		$abv = (1.05/0.79) * (($startingGravity - $finalGravity)/$finalGravity) * 100;
		return number_format(round($abv, 1), 1) . '%';
	}	
	
}	