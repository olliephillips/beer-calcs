<!DOCTYPE html>
<html lang="en-us">
    <head>
        <meta charset="utf-8">
        <title>Beer Calcs</title>
        <meta name="description" content="Get it on your smartphone!">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
		<script src="https://d10ajoocuyu32n.cloudfront.net/jquery-1.9.1.min.js"></script>
		<style type="text/css">
			body {background:#FFCC00;}	
			h1{font-family:serif; text-align:center;text-transform:uppercase;text-shadow: 1px 1px #fbffab;}
			h2{font-family:serif; text-align:center;text-transform:uppercase;text-shadow: 1px 1px #fbffab;font-size:14px;}
			.adjIcon{margin-top:10px;}
			.adjTitle{margin-top:-30px;}
			.padQr {margin-top:30px;}
			.icon-beer {font-size:200px!important;text-shadow: 2px 2px #fbffab;}
		
			.boxShadow {border: 1px solid #000000; box-shadow: 2px 2px #fbffab;}
			#wrap{width:520px;margin:200px auto 0 auto;}
			#icon{float:left;margin-left:20px;width:240px;}
			#qr{float:right;}
		</style>	
	</head>	
    <body>
	<div id="wrap">
		<div id="icon">
			<div class="adjIcon">
				<i class="icon-beer"></i>
			</div>
			<div class="adjTitle">
				<h1>Beer Calcs</h1>
			</div>
		</div>
		<div id="qr">
			<div class="centerIt">
				<img id="qrcode" class="boxShadow" src="http://qrfree.kaywa.com/?l=1&amp;s=8&amp;d=http%3A%2F%2Fbeercalcs.eantics.co.uk" alt="Get it on your smartphone"/>
			</div>	
		</div>
		<div style="clear:both;margin-bottom:20px;"></div>
		<h2>Get it on your smartphone!</h2>
	</div>
	</body>
</html>