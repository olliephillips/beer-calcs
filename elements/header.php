<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <title>Beer Calcs</title>
  <link rel="stylesheet" href="https://d10ajoocuyu32n.cloudfront.net/mobile/1.3.1/jquery.mobile-1.3.1.min.css">
  <link rel="stylesheet" href="css/styles.css">
  <!-- jQuery and jQuery Mobile -->
  <script src="https://d10ajoocuyu32n.cloudfront.net/jquery-1.9.1.min.js"></script>
  <script src="https://d10ajoocuyu32n.cloudfront.net/mobile/1.3.1/jquery.mobile-1.3.1.min.js"></script>
  <link rel="apple-touch-icon" href="apple-touch-icon.png" />
  <link rel="apple-touch-icon" href="apple-touch-icon-iphone.png" />
  <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-ipad.png" />
  <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-iphone-retina-display.png" />
  <script type="text/javascript" src="js/add2home.js" charset="utf-8"></script>
</head>
<body>
<!-- Home -->
<div data-role="page" id="page1">
    <div data-theme="a" data-role="header">
        <h3>
            Beer Calcs
        </h3>
		<div data-role="fieldcontain">
            <select id="calculator" name="calculator">
				<optgroup label="Calculators">
             	   <option value="sgc">
	                    Specific Gravity Correction
	                </option>
	 				<option value="abv" selected>
	                    Alcohol By Volume (ABV%)
	                </option>
					<option value="att">
	                    Yeast Attenuation
	                </option>
				</optgroup>
				<!--
				<optgroup label="Data">
             	   <option value="gra">
	                    Grains
	                </option>
	 				<option value="hop">
	                    Hops
	                </option>
					<option value="yea">
	                    Yeasts
	                </option>
				</optgroup>
				-->
            </select>
        </div>
     </div>
	<div data-role="content">
		