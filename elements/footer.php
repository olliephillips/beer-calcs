	</div>
</div>
<script type="text/javascript">

$(document).bind("pageinit", function(){
	// Nav
	
	$('#calculator').on('change', function(e){
		var calc = $(this).val();
		var app;
		switch(calc){
			case 'sgc':
				app = 'specificGravityCorrection';
			break;
			case 'abv':
				app = 'alcoholByVolume';
			break;
			case 'att':
				app = 'yeastAttenuation';
			break; 
		}
		
		$.ajax({
		  	url: "calculators/" + app + '.php',
			data:'',
		  	success: function(response) {
				$("#calcspace").html(response);
				$("#calcspace").trigger('create');
			},
			complete: function() {
				bind();
			}	
		});
		
	});
	
	$(document).ajaxStart(function() {
	    $.mobile.loading('show');
	});

	$(document).ajaxStop(function() {
	    $.mobile.loading('hide');
	});

});
</script>
</body>
</html>