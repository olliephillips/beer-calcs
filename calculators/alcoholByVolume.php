
		<div data-role="fieldcontain">
		    <label for="startinggravity">
		        Original Gravity
		    </label>
		    <input id="startinggravity" name="startinggravity" value="1040" min="1020" max="1095"
		    data-highlight="true"  type="range">
		</div>
		<div data-role="fieldcontain">
		    <label for="finalgravity">
		        Final Gravity
		    </label>
		    <input id="finalgravity" name="finalgravity" value="1010" min="1005" max="1020" data-highlight="true"
		    type="range">
		</div>
		<div data-role="fieldcontain">
			<div id="calcresult">
		  	  	<h3>
		            Alcohol By Volume (ABV) 
		        </h3>
				<h2 id="result">
						3.9%
				</h2>	
			</div>	
		</div>

		<script type="text/javascript">
			// ABV 
			var updateABV = function(){
		
				var startingGravity = $('#startinggravity').val();
				var finalGravity = $('#finalgravity').val();
				$.ajax({
				  	url: "ajax/alcoholByVolume.php?startinggravity=" + escape(startingGravity) + "&finalgravity=" + escape(finalGravity),
					data:'',
				  	success: function(response) {
						$("#result").html(response);
					}
				});
			}
	
			var bind = function(){
		
				$('#startinggravity').on('slidestop', function(){
					updateABV();
				});
		
				$('#finalgravity').on('slidestop', function(){
					updateABV();
				});
		
			}
	
			$(document).bind("pageinit", function(){	
				bind();			
			});	
		</script>