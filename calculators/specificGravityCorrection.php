 		
		<div data-role="fieldcontain">
            <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">       
                <input id="unitsc" name="units" value="c" data-theme="c" type="radio" checked>
                <label for="unitsc">
                    &deg;C
                </label>
                <input id="unitsf" name="units" value="f" data-theme="c" type="radio">
                <label for="unitsf">
                    &deg;F
                </label>
            </fieldset>
        </div>
        <div data-role="fieldcontain">
            <label for="hydrometer">
                Hydrometer Reading
            </label>
            <input id="hydrometer" name="hydrometer" value="1040" min="1005" max="1095"
            data-highlight="true"  type="range">
        </div>
        <div data-role="fieldcontain">
            <label for="temperature">
                Temperature
            </label>
            <input id="temperature" name="temperature" value="20" min="15" max="40" data-highlight="true"
            type="range">
        </div>
        <div data-role="fieldcontain">
			<div id="calcresult">
          	  	<h3>
	                Specific Gravity (adjusted)
	            </h3>
				<h2 id="result">
						1.040
				</h2>	
			</div>	
        </div>

		<script type="text/javascript">
			// Standard Gravity Correction
			
			var update = function(){
				var temp = $('#temperature').val();
				var unitsSelected = $("input[type='radio'][name='units']:checked");
				var units = unitsSelected.val();
				var hydrometer = $('#hydrometer').val();
				$.ajax({
				  	url: "ajax/specificGravityCorrection.php?temperature=" + escape(temp) + "&units=" + escape(units) + "&hydrometer=" + escape(hydrometer),
					data:'',
				  	success: function(response) {
						$("#result").html(response);
					}
				});
			}	
			
			var bind = function() {
				$('#unitsc').on("click", function(){
					// Make Celsuis
					var curVal = 20;
					var maxVal = 40;
					var minVal = 15;
					$('#temperature').attr('min',minVal);
					$('#temperature').attr('max',maxVal);
					$('#temperature').val(curVal).slider("refresh");
					update();	
				});

				$('#unitsf').on("click", function(){
					// Make Farenheit
					var curVal = 68;
					var maxVal = 104;
					var minVal = 59;
					$('#temperature').attr('min',minVal);
					$('#temperature').attr('max',maxVal);
					$('#temperature').val(curVal).slider("refresh");
					update();				
				});

				$('#hydrometer').on('slidestop', function(){
					update();
				});

				$('#temperature').on('slidestop', function(){
					update();
				});
			}	
			
			$(document).bind("pageinit", function(){	
				bind();		
			});
		</script>	