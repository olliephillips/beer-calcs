	<div data-role="fieldcontain">
		<label for="lab">
	        Yeast Strain
	    </label>
		<?php 
		require('../libraries/yeastAttenuation.php');
		$yeastOptions = yeastAttenuation::getYeastOptions();
		?>
	    <select id="yeast" name="yeast" data-mini="true">
			<?php echo $yeastOptions;?>
	    </select>
	</div>
	<div data-role="fieldcontain">
	    <label for="startinggravity">
	        Original Gravity
	    </label>
	    <input id="startinggravity" name="startinggravity" value="1040" min="1020" max="1095"
	    data-highlight="true" type="range">
	</div>
	<div data-role="fieldcontain">
	    <label for="finalgravity">
	        Final Gravity
	    </label>
	    <input id="finalgravity" name="finalgravity" value="1010" min="1005" max="1020" data-highlight="true"
	    type="range">
	</div>
	<div data-role="fieldcontain">
		<div id="calcresult">
	  	  	<h3>
	            Attenuation 
	        </h3>
			<h2 id="target">	
				Expected: <span id="target-data">High</span>	
			</h2>
			<h2 id="actual">
				Actual: <span id="actual-data">75%</span>
			</h2>		
		</div>	
	</div>

	<script type="text/javascript">
		// Set Expected data for yeast
		$('#yeast').change(function(e){
			$('#target-data').html($(this).val());
		});
		
		var updateAttenuation = function(){
	
			var startingGravity = $('#startinggravity').val();
			var finalGravity = $('#finalgravity').val();
			$.ajax({
			  	url: "ajax/yeastAttenuation.php?startinggravity=" + escape(startingGravity) + "&finalgravity=" + escape(finalGravity),
				data:'',
			  	success: function(response) {
					$("#actual-data").html(response);
				}
			});
		}

		var bind = function(){
	
			$('#startinggravity').on('slidestop', function(){
				updateAttenuation();
			});
	
			$('#finalgravity').on('slidestop', function(){
				updateAttenuation();
			});
	
		}

		$(document).bind("pageinit", function(){	
			bind();			
		});	
	</script>